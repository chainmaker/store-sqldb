/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
package rawsqlprovider

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"

	"chainmaker.org/chainmaker/protocol/v2"
	"github.com/stretchr/testify/assert"
)

// UpdateBatch encloses the details of multiple `updates`
type UpdateBatch struct {
	kvs map[string][]byte
}

func (batch *UpdateBatch) Remove(key []byte) {
	delete(batch.kvs, string(key))
}

func (batch *UpdateBatch) Get(key []byte) ([]byte, error) {
	v, ok := batch.kvs[string(key)]
	if ok {
		return v, nil
	}
	return nil, nil
}

func (batch *UpdateBatch) Has(key []byte) bool {
	_, ok := batch.kvs[string(key)]
	return ok
}

func (batch *UpdateBatch) SplitBatch(batchCnt uint64) []protocol.StoreBatcher {
	panic("implement me")
}

// NewUpdateBatch constructs an instance of a Batch
func NewUpdateBatch() protocol.StoreBatcher {
	return &UpdateBatch{kvs: make(map[string][]byte)}
}

// KVs return map
func (batch *UpdateBatch) KVs() map[string][]byte {
	return batch.kvs
}

// Put adds a KV
func (batch *UpdateBatch) Put(key []byte, value []byte) {
	if value == nil {
		panic("Nil value not allowed")
	}
	batch.kvs[string(key)] = value
}

// Delete deletes a Key and associated value
func (batch *UpdateBatch) Delete(key []byte) {
	batch.kvs[string(key)] = nil
}

// Len returns the number of entries in the batch
func (batch *UpdateBatch) Len() int {
	return len(batch.kvs)
}

// Merge merges other kvs to this updateBatch
func (batch *UpdateBatch) Merge(u protocol.StoreBatcher) {
	for key, value := range u.KVs() {
		batch.kvs[key] = value
	}
}

var confKvTest = &SqlDbConfig{
	SqlDbType: "sqlite",
	Dsn:       filepath.Join(os.TempDir(), fmt.Sprintf("%d_unit_test_db", time.Now().UnixNano())+":memory:"),
}

var (
	key1         = []byte("key1")
	value1       = []byte("value1")
	key2         = []byte("key2")
	value2       = []byte("value2")
	key3         = []byte("key3")
	value3       = []byte("value3")
	key4         = []byte("key4")
	value4       = []byte("value4")
	key5         = []byte("key5")
	value5       = []byte("value5")
	key6         = []byte("key6")
	keyFilter1   = []byte("keyFilter1")
	valueFilter1 = []byte("valueFilter1")
	keyFilter2   = []byte("keyFilter2")
	valueFilter2 = []byte("valueFilter2")
	keyFilter3   = []byte("keyFilter3")
	valueFilter3 = []byte("valueFilter3")
)
var op = &NewSqlDBOptions{
	Config:    confKvTest,
	Logger:    log,
	Encryptor: nil,
	ChainId:   "test-chain1",
	DbName:    "dbName1",
}

func TestSqlDBHandle_NewIteratorWithPrefix(t *testing.T) {
	dbHandle := NewKVDBHandle(op)
	defer dbHandle.Close()
	//
	//err := dbHandle.CreateTableIfNotExist(&KeyValue{})
	//assert.Nil(t, err)

	err := dbHandle.Put(keyFilter1, valueFilter1)
	assert.Nil(t, err)
	err = dbHandle.Put(keyFilter2, valueFilter2)
	assert.Nil(t, err)
	err = dbHandle.Put(keyFilter3, valueFilter3)
	assert.Nil(t, err)

	res, err := dbHandle.NewIteratorWithPrefix([]byte("keyFilter"))
	assert.Nil(t, err)
	//key := res.Key()
	//assert.Equal(t, true, strings.Contains(string(keyFilter1)+string(keyFilter2)+string(keyFilter3), string(key)))
	//value := res.Value()
	//assert.Equal(t, true, strings.Contains(string(valueFilter1)+string(valueFilter2)+string(valueFilter3), string(value)))

	count := 0
	for res.Next() {
		count++
		t.Logf("key%s,value:%s", res.Key(), res.Value())
	}
	assert.Equal(t, 3, count)
	fmt.Println(res)

	err = res.Error()
	assert.Nil(t, err)
	res.Release()
	assert.False(t, res.Next())
}

func TestSqlDBHandle_NewIteratorWithRange(t *testing.T) {
	dbHandle := NewKVDBHandle(op)
	//defer dbHandle.Close()

	//err := dbHandle.CreateTableIfNotExist(&KeyValue{})
	//assert.Nil(t, err)

	err := dbHandle.Put(key3, value3)
	assert.Nil(t, err)
	err = dbHandle.Put(key4, value4)
	assert.Nil(t, err)
	err = dbHandle.Put(key5, value5)
	assert.Nil(t, err)

	res, err := dbHandle.NewIteratorWithRange(key3, key5)
	assert.Nil(t, err)
	count := 0
	t.Logf("key%s,value:%s,err:%s", res.Key(), res.Value(), res.Error())
	for res.Next() {
		count++
		t.Logf("key%s,value:%s", res.Key(), res.Value())
	}
	assert.Equal(t, 2, count)
	fmt.Println(res)

	res, err = dbHandle.NewIteratorWithRange([]byte(""), []byte(""))
	assert.Nil(t, res)
	assert.NotNil(t, err)

	dbHandle.Close()
	res, err = dbHandle.NewIteratorWithRange(key3, key6)
	assert.Nil(t, res)
	assert.Nil(t, err)
}

func TestSqlDBHandle_Put(t *testing.T) {
	dbHandle := NewSqlDBHandle(op)
	//defer dbHandle.Close()

	err := dbHandle.CreateTableIfNotExist(&KeyValue{})
	assert.Nil(t, err)

	err = dbHandle.Put(key1, value1)
	assert.Nil(t, err)

	res, err := dbHandle.Get(key1)
	assert.Nil(t, err)
	assert.Equal(t, string(value1), string(res))

	has, err := dbHandle.Has(key1)
	assert.True(t, has)
	assert.Nil(t, err)

	err = dbHandle.Delete(key1)
	assert.Nil(t, err)

	err = dbHandle.Delete(key1)
	assert.NotNil(t, err)

	res, err = dbHandle.Get(key2)
	assert.Nil(t, err)
	assert.Nil(t, res)

	dbHandle.Close()
	_, err = dbHandle.Get(key2)
	assert.NotNil(t, err)

	has, err = dbHandle.Has(key2)
	assert.False(t, has)
	assert.NotNil(t, err)

	err = dbHandle.Delete(key1)
	assert.NotNil(t, err)
}

func TestSqlDBHandle_WriteBatch(t *testing.T) {
	dbHandle := NewKVDBHandle(op)
	defer dbHandle.Close()

	batch := NewUpdateBatch()

	batch.Put(key1, value1)
	batch.Put(key2, value2)
	batch.Put(key2, value1)
	batch.Delete([]byte("no key found"))
	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	//dbHandle.Close()
	//
	//err = dbHandle.WriteBatch(batch, true)
	//assert.NotNil(t, err)
}

func TestSqlDBHandle_Gets(t *testing.T) {
	dbHandle := NewKVDBHandle(op)
	defer dbHandle.Close()

	n := 3
	keys := make([][]byte, 0, n+1)
	values := make([][]byte, 0, n+1)
	batch := NewUpdateBatch()
	for i := 0; i < n; i++ {
		keyi := []byte(fmt.Sprintf("key%d", i))
		valuei := []byte(fmt.Sprintf("value%d", i))
		keys = append(keys, keyi)
		values = append(values, valuei)

		batch.Put(keyi, valuei)
	}

	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	keys = append(keys, nil)
	values = append(values, nil)
	valuesR, err1 := dbHandle.GetKeys(keys)
	assert.Nil(t, err1)
	for i := 0; i < len(keys); i++ {
		assert.Equal(t, values[i], valuesR[i])
	}
}

func Test_deleteInTx(t *testing.T) {
	dbHandle := NewKVDBHandle(op)
	//defer dbHandle.Close()

	tx, err := dbHandle.BeginDbTransaction("1234567890")
	assert.Nil(t, err)

	err = dbHandle.Put(key1, value1)
	assert.Nil(t, err)
	err = deleteInTx(tx, key1, log)
	assert.Nil(t, err)

	err = deleteInTx(tx, key1, log)
	assert.NoError(t, err)

	dbHandle.Close()

	err = deleteInTx(tx, key1, log)
	assert.Error(t, err)
}
