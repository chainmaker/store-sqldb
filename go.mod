module chainmaker.org/chainmaker/store-sqldb/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.4
	chainmaker.org/chainmaker/protocol/v2 v2.3.5
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.1.2
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/stretchr/testify v1.7.0
)
